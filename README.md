# Force Field

Theme: Blast away bad guys

Genre: Hypercasual

Sub-Genre: Hypercasual - Arcade

Short Description:
● Generate force fields and release them to destroy the required number
of bad guys. Tap and hold to generate force fields while avoiding bad
guys, release to destroy the bad guys.

References:
1. Reference
a. URL: https://www.youtube.com/watch?v=PFauRdA1v3Y 
b. Description: Old Flash game where you click and hold to blow a
bubble. Release to pop the bubble and destroy all of the bugs in the
bubble. If your bubble touches any bugs while it's being blown, you
lose a lose.

Reference Art Style 
3D-Low Poly 
Hypercasual low poly blue and red characters
https://apps.apple.com/app/id1562817072

Reference Camera Orientation: Top Down
Reference Controls: Tap - Hold
tap to generate the force field, release to activate
the force field and destroy the bad guys within

Game Loop
Loop Progression
Each level consists of your character, with a bunch of enemies running
around
If your force field is hit by an enemy while being created, you lose a life. Lose
all lives and the level is failed
Destroy enough enemies to complete the level
You can upgrade your force field generation speed, initial diameter, and life
count
Progression & Upgrades
Type: level based
Description:
Number of bad guys running around
Number of bad guys required to destroy to complete level
Type: upgrade based
Description:
Can upgrade your force field generation speed, initial force field diameter,
and the number of lives
Other Attachments
● n/a