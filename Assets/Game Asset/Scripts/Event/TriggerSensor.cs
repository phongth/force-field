using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public class TriggerSensor : BaseMono
    {
        [SerializeField] private UnityEvent<GameObject, Collider> onTriggerEnter;

        private void OnTriggerEnter(Collider other)
        {
            onTriggerEnter?.Invoke(CacheGameObject, other);
        }
    }
}

