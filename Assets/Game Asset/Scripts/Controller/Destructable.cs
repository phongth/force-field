using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;

namespace Game
{
    public class Destructable : BaseMono
    {
        [SerializeField] private Rigidbody[] fragments;

        public void Explode(float force, Vector3 position, float radius, float timeDestroy = 2f, bool useGravity = true)
        {
            for (int i = 0; i < fragments.Length; ++i)
            {
                fragments[i].useGravity = true;
                fragments[i].AddExplosionForce(force, position, radius);
            }
            
            Destroy(CacheGameObject, timeDestroy);
        }
    }
}

