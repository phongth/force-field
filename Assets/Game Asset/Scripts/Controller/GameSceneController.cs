using Base;
using Base.MessageSystem;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameSceneController : BaseMono
    {
        [SerializeField] private LevelController levelPrefab;
        private LevelController _levelInstance;
        public LevelController LevelController => _levelInstance;

        private GameStateController _gameStateController;
        
        public int GameLevel { get; set; }

        private void Start()
        {
            Scene managerScene = SceneManager.GetSceneByBuildIndex(0);
            
            SceneManager.SetActiveScene(SceneManager.GetSceneByName("Game Scene"));
            Messenger.RegisterListener<bool>(EventID.Replay, OnReplayHandler);

            var rootGameObjects = managerScene.GetRootGameObjects();
            _gameStateController = rootGameObjects[0].GetComponent<GameStateController>();
            
            GameLevel = PlayerPrefs.GetInt("GameLevel", 1);

            OnReplayHandler(false);
        }

        protected override void OnDestroy()
        {
            if (_levelInstance && _gameStateController)
            {
                _gameStateController.OnStateChanged -= _levelInstance.OnGameStatedChanged;
            }
            Messenger.RemoveListener<bool>(EventID.Replay, OnReplayHandler);
        }

        private async void OnReplayHandler(bool isWin)
        {
            if (_levelInstance)
            {
                _gameStateController.OnStateChanged -= _levelInstance.OnGameStatedChanged;
                Destroy(_levelInstance.gameObject);
                
                PlayerPrefs.SetInt("GameLevel", GameLevel);
                if (isWin) GameLevel++;
            }

            await UniTask.NextFrame();
            
            _levelInstance = Instantiate(levelPrefab, Vector3.zero, Quaternion.identity);
            _levelInstance.Active = true;
            _levelInstance.Initialize(this);
            _gameStateController.OnStateChanged += _levelInstance.OnGameStatedChanged;
        }
    }
}

