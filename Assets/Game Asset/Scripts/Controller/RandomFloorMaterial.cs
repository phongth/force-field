using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game
{
    public class RandomFloorMaterial : MonoBehaviour
    {
        [SerializeField] private Material[] materials;
        private MeshRenderer _renderer;

        private void Start()
        {
            _renderer = GetComponent<MeshRenderer>();

            int index = Random.Range(0, materials.Length);
            _renderer.sharedMaterial = materials[index];
        }
    }
}

