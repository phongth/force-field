using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class IgnoreCollision : MonoBehaviour
    {
        [SerializeField] private Collider one;
        [SerializeField] private Collider two;

        private void Start()
        {
            Physics.IgnoreCollision(one, two, true);
        }
    }
}

