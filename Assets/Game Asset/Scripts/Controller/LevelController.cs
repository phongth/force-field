using System;
using System.Collections.Generic;
using System.Linq;
using Base;
using Base.Pattern;
using UltimateSpawner;
using UltimateSpawner.Spawning;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;

namespace Game
{
    public class LevelController : BaseMono
    {
        [SerializeField] private SpawnPoint spawnPoint;
        [SerializeField] private InfiniteSpawnController infiniteSpawnController;
        [SerializeField] private EventSpawnController eventSpawnController;
        [SerializeField] private BaseMono mainCharacter;
        [SerializeField] private NavigationIndicator navigationIndicator;
        [SerializeField] private Transform whereToPutInstantiateObject;
        private int _totalNpcCount = 0;
        private int _totalNpc1Count = 0;
        private int _totalNpc2Count = 0;

        private int _gameLevel = 1;

        public int TotalNpcDie => _totalNpcCount;

        private void Awake()
        {
            UltimateSpawning.OnUltimateSpawnerInstantiate += HandleSpawnerInstantiate;
            UltimateSpawning.OnUltimateSpawnerDestroy += HandleSpawnerDestroy;
        }

        private void Start()
        {
            Application.targetFrameRate = 60;
            _totalNpcCount = 0;
            
            eventSpawnController.StartSpawning();
        }

        private void LateUpdate()
        {
            navigationIndicator.UpdateIndicator();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            UltimateSpawning.OnUltimateSpawnerInstantiate -= HandleSpawnerInstantiate;
            UltimateSpawning.OnUltimateSpawnerDestroy -= HandleSpawnerDestroy;
        }

        private void MaskNpcSpawnItem(int spawnableId)
        {
            // SpawnableMask mask = infiniteSpawnController.spawner.SpawnableMask;
            // mask.UnmaskItem(spawnableId);
        }

        private Object HandleSpawnerInstantiate(Object prefab, Vector3 position, Quaternion rotation)
        {
            if (ObjectPooler.Include(prefab.name))
            {
                Transform obj = ObjectPooler.Get(prefab.name, position, rotation.eulerAngles, whereToPutInstantiateObject);
                return obj.gameObject;
            }

            var resultObj = Instantiate(prefab as GameObject, position, rotation, whereToPutInstantiateObject);
             return resultObj;
        }

        private void HandleSpawnerDestroy(Object obj)
        {
            string desiredKey = "Npc Template 1";
            string key = obj.name.Remove(desiredKey.Length, "(Clone)".Length);
            if (ObjectPooler.Include(key))
            {
                ObjectPooler.Return(key, (obj as GameObject)?.transform);
            }
        }

        public void OnItemDespawned(Transform item)
        {
            int totalNpcBeforeBoss = Constant.NumOfEnemyBeforeBoss * _gameLevel;
            _totalNpcCount++;
            if (_totalNpcCount == totalNpcBeforeBoss && GameStateParam.IsGameRunning)
            {
                SpawnBoss();
            }

            // NpcRanking ranking = item.GetComponent<NpcRanking>();
            // if (ranking)
            // {
            //     if (ranking.npcType == NpcType.NormalNpc)
            //     {
            //         if (ranking.npcNormalType == NpcNormalType.Npc1) _totalNpc1Count++;
            //         else if (ranking.npcNormalType == NpcNormalType.Npc2) _totalNpc2Count++;
            //     }
            // }
            //
            // if (_totalNpc1Count >= totalNpcBeforeBoss * .5f)
            // {
            //     MaskNpcSpawnItem((int) NpcNormalType.Npc2);
            // }
            //
            // if (_totalNpc2Count >= totalNpcBeforeBoss * .7f)
            // {
            //     MaskNpcSpawnItem((int) NpcNormalType.Npc3);
            // }
        }
        public void SpawnBoss()
        {
            Transform result = spawnPoint.Spawn();
            if (result != null)
            {
                navigationIndicator.SetTarget(result, mainCharacter);
                navigationIndicator.Active = true;
            }
        }

        public void OnGameStatedChanged(GameState from, GameState to)
        {
            if (to is GameRunningState)
            {
                infiniteSpawnController.StartSpawning();
            }
            else if (to is GameEndState)
            {
                infiniteSpawnController.StopSpawning();
            }
        }

        public void Initialize(GameSceneController gameSceneController)
        {
            _gameLevel = gameSceneController.GameLevel;
        }
    }
}

