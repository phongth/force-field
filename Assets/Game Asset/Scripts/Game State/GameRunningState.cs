using System;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameRunningState : GameState
    {
        private bool _isInputBlocked = true;
        private bool _isGameOver = false;
        private bool _isReturnHome = false;
        private InputAction inputAction => GameStateController.InputAction;
        
        public bool IsGameWin { get; private set; }

        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            GameStateParam.IsGameRunning = true;
            _isInputBlocked = true;
            
            Messenger.RegisterListener<bool>(EventID.GameOver, OnGameOver);
            Messenger.RegisterListener(EventID.ToHome, OnToHome);
        }

        public override void UpdateBehaviour(float dt)
        {
            if (_isInputBlocked) if (inputAction.Phase == InputPhase.Began) _isInputBlocked = false;
            
            if (!_isInputBlocked)
            {
                Messenger.RaiseMessage(SystemMessage.Input, inputAction.Position, 
                    inputAction.Phase);
            }
        }

        public override void CheckExitTransition()
        {
            if (_isGameOver) GameStateController.EnqueueTransition<GameEndState>();
            else if (_isReturnHome) GameStateController.EnqueueTransition<GameInitializeState>();
        }

        public override void ExitStateBehaviour(float dt, GameState toState)
        {
            GameStateParam.IsGameRunning = false;
            
            Messenger.RemoveListener<bool>(EventID.GameOver, OnGameOver);
            Messenger.RemoveListener(EventID.ToHome, OnToHome);

            _isGameOver = false;
            _isReturnHome = false;

            if (toState is GameInitializeState)
            {
                UnloadSceneAsync("Game Scene").Forget();
            }
        }

        private void OnApplicationQuit()
        {
            GameStateParam.IsGameRunning = false;
        }

        private async UniTaskVoid UnloadSceneAsync(string sceneName)
        {
            await SceneManager.UnloadSceneAsync(sceneName);
        }

        private void OnGameOver(bool isWin)
        {
            _isGameOver = true;
            IsGameWin = isWin;
        }

        private void OnToHome()
        {
            _isReturnHome = true;
        }
    }
}

