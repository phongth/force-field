using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameInitializeState : GameState
    {
        private bool _toRunningState;
        private InputAction InputAction => GameStateController.InputAction;
        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            if (!SceneManager.GetSceneByName("Game Scene").isLoaded)
            {
                LoadSceneAsync("Game Scene").Forget();
            }
        }

        public override void UpdateBehaviour(float dt)
        {
            if (InputAction.Phase == InputPhase.Began)
            {
                _toRunningState = true;
            }
        }

        public override void CheckExitTransition()
        {
            if (_toRunningState) GameStateController.EnqueueTransition<GameRunningState>();
        }

        public override void ExitStateBehaviour(float dt, GameState toState)
        {
            _toRunningState = false;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            Messenger.CleanUp();
        }

        private async UniTaskVoid LoadSceneAsync(string sceneName)
        {
            await SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        }
    }
}

