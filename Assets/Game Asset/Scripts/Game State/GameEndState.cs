using System.Collections;
using System.Collections.Generic;
using Base.MessageSystem;
using Base.Pattern;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameEndState : GameState
    {
        private bool _toInitializeState = false;
        private bool _toRunningState = false;
        public override void EnterStateBehaviour(float dt, GameState fromState)
        {
            Messenger.RegisterListener<bool>(EventID.Replay, OnReplayHandler);
        }

        public override void UpdateBehaviour(float dt)
        {
            return;
        }

        public override void CheckExitTransition()
        {
            if (_toInitializeState) GameStateController.EnqueueTransition<GameInitializeState>();
            else if (_toRunningState) GameStateController.EnqueueTransition<GameRunningState>();
        }

        public override void ExitStateBehaviour(float dt, GameState toState)
        {
            Messenger.RemoveListener<bool>(EventID.Replay, OnReplayHandler);
            
            //UnloadSceneAsync("Game Scene").Forget();

            _toRunningState = false;
            _toInitializeState = false;
        }

        private void OnReplayHandler(bool isWin)
        {
            _toInitializeState = true;

            //_toRunningState = true;
        }

        private async UniTaskVoid UnloadSceneAsync(string sceneName)
        {
            await SceneManager.UnloadSceneAsync(sceneName);
            
            _toInitializeState = false;
        }
    }
}

