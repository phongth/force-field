using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Base;
using UnityEngine;

namespace Game
{
    public class MovementControl : BaseMono
    {
        public enum ControlType {Rigidbody, CharacterController}

        [SerializeField] private ControlType controlType;
        [SerializeField] private PlayerParams property;
        [SerializeField] private ForceControl forceControl;

        private float _jumpMultiplier = 2.5f;
        private float _fallMultiplier = 2.5f;

        private Rigidbody _rigidbody;
        private CharacterController _characterController;
        private Vector3 _jumpMotion = Vector3.zero;

        private Vector3 _lookDir;
        private bool _onGround = false;

        private float _maxScale = 2f;

        public event Action OnLanding;

        private bool IsGround
        {
            get => _onGround;
            set
            {
                var onGround = _onGround;
                if (onGround != value)
                {
                    _onGround = value;
                    if (value) OnLanding?.Invoke();
                }
            }
        }

        private void Start()
        {
            if (controlType == ControlType.Rigidbody) _rigidbody = GetComponent<Rigidbody>();
            else if (controlType == ControlType.CharacterController) _characterController = GetComponent<CharacterController>();
        }

        private void FixedUpdate()
        {
            GroundCheck();
        }

        private void Update()
        {
            if (_rigidbody.velocity.y < 0)
            {
                _rigidbody.velocity += Vector3.up * Physics.gravity.y * (_fallMultiplier - 1f) * Time.deltaTime;
            }
            else if (_rigidbody.velocity.y > 0)
            {
                _rigidbody.velocity += Vector3.up * Physics.gravity.y * (_jumpMultiplier - 1f) * Time.deltaTime;
            }
        }

        private void GroundCheck()
        {
            IsGround = Physics.CheckSphere(Position, .15f, LayerMask.GetMask("Ground"));
        }

        public void ExecuteMove()
        {
            if (controlType == ControlType.Rigidbody)
            {
                Vector3 rigidVelocity = _rigidbody.velocity;
                Vector3 motion = CacheTransform.forward * (property.moveSpeed * Time.deltaTime * forceControl.SpeedDamp);
                _rigidbody.velocity = new Vector3(motion.x, rigidVelocity.y, motion.z);
            }
            else if (controlType == ControlType.CharacterController)
            {
                Vector3 previousVelo = _characterController.velocity;
                Vector3 motion = CacheTransform.forward * (property.moveSpeed * Time.fixedDeltaTime);
                _characterController.Move(new Vector3(motion.x, previousVelo.y, motion.z) + _jumpMotion);
            }
        }

        public void ExecuteDrag(Vector3 endPos)
        {
            Vector3 end = new Vector3(endPos.x, _rigidbody.position.y, endPos.z);
            _rigidbody.velocity = end.normalized * property.moveSpeed * Time.fixedDeltaTime;
        }

        public void ExecuteRotate(Vector3 deltaClick, Vector3 lookDir)
        {
            if (deltaClick != Vector3.zero)
            {
                Vector3 deltaTouch = deltaClick;
                deltaTouch.z = deltaTouch.y;
                deltaTouch.y = 0;

                lookDir += deltaTouch;
                lookDir = Vector3.ClampMagnitude(lookDir, 150f);
            }
                
            if (lookDir != Vector3.zero)
            {
                float angle = Mathf.Atan2(lookDir.x, lookDir.z) * Mathf.Rad2Deg;
                //float smoothAngle = Mathf.SmoothDampAngle(transform.eulerAngles.y, angle, ref _turnSmoothTime, speedLerpRotate);
                //transform.rotation = Quaternion.Euler(0f, smoothAngle, 0);
                //_playerController.Rotation = Quaternion.Lerp(_playerController.Rotation, Quaternion.Euler(0f, angle, 0), speedLerpRotate);
                    
                //_playerController.CharacterRotate(Quaternion.Lerp(_playerController.Rotation, Quaternion.Euler(0f, angle, 0), speedLerpRotate));
                
                if(controlType == ControlType.Rigidbody)
                    _rigidbody.rotation = Quaternion.Lerp(Rotation, Quaternion.Euler(0f, angle, 0f), property.speedLerpRotate);
                else if (controlType == ControlType.CharacterController)
                    Rotation =  Quaternion.Lerp(Rotation, Quaternion.Euler(0f, angle, 0f), property.speedLerpRotate);
            }
        }

        public void ExecuteJump(Vector3 forceMag)
        {
            if (controlType == ControlType.Rigidbody)
            {
                if (IsGround)
                {
                    float forceMagnitude = forceMag.magnitude / 20f;
                    Vector3 forward = CacheTransform.forward;
                    Vector3 jumpVel = new Vector3(forward.x * property.jumpSpeed * forceMagnitude, 
                        property.jumpSpeed * Constant.ForceMultiplier + forceMagnitude * 5f,
                        forward.z * property.jumpSpeed * forceMagnitude);
                    _rigidbody.velocity = jumpVel;
                }
            }
            else if (controlType == ControlType.CharacterController)
            {
                if (IsGround)
                {
                    float forceMagnitude = forceMag.magnitude / 20f;
                    Vector3 forward = CacheTransform.forward;
                    Vector3 jumpVel = new Vector3(forward.x * property.jumpSpeed * forceMagnitude, 
                        property.jumpSpeed * Constant.ForceMultiplier + forceMagnitude * 5f,
                        forward.z * property.jumpSpeed * forceMagnitude);
                    //_characterController.velocity = jumpVel;

                    _jumpMotion = jumpVel;
                }
            }
        }

        public void ResetScale()
        {
            Scale = Vector3.one;
        }
    }
}

