using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using UnityEngine;

namespace Game
{
    public class ForceReleaseObject : BaseMono
    {
        [SerializeField] private Transform child;
        [SerializeField] private Destructable brokenPrefab;

        public void Explosion(float force)
        {
            var position = child.position;
            var broken = Instantiate(brokenPrefab, position, brokenPrefab.Rotation);
            broken.Scale = Scale;
            broken.Explode(force, position, 25f);
            Destroy(broken.gameObject, .25f);
        }
    }
}

