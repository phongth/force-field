using System;
using Base.MessageSystem;
using DG.Tweening;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class NpcWanderingState : CharacterState
    {
        [SerializeField] private NpcMovementControl movementControl;

        private bool _toDieState;
        private bool _toIdleState;
        private Transform _playerTransform;
        private NpcRanking _ranking;
        private Rigidbody _body;

        private bool _isPushBack;
        private float _distance;

        public Tuple<Vector3, float> forceInfo;

        private static int _blendMove = Animator.StringToHash("Blend_Move");
        private static int _hitButNotDead = Animator.StringToHash("hitButNotDead");

        private Transform PlayerTransform
        {
            get
            {
                if (_playerTransform == null)
                {
                    _playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
                }

                return _playerTransform;
            }
            set => _playerTransform = value;
        }

        protected override void Start()
        {
            _ranking = CharacterStateController.GetComponent<NpcRanking>();
            _body = CharacterStateController.GetComponent<Rigidbody>();
        }

        public override void EnterState(float dt, CharacterState fromState)
        {
            Messenger.RegisterListener<int, Vector3, float>(EventID.DetectNpc, OnDetected);
        }

        public override void UpdateBehaviour(float dt)
        {
            if (!GameStateParam.IsGameRunning)
            {
                _toIdleState = true;
                return;
            }

            if (!_isPushBack)
            {
                Vector3 direction = PlayerTransform.position - CharacterStateController.Position;
                direction.y = 0;
                _distance = direction.magnitude;
                float angle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
                if (_distance <= movementControl.StoppingDistance)
                {
                    movementControl.StopMove();
                }
                else
                {
                    movementControl.ExecuteRotate(angle);
                    movementControl.ExecuteMove(direction);
                }
            }
        }

        public override void FixedUpdateBehaviour(float dt)
        {
            return;
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (CharacterStateController.Animator == null) return;
            if (!CharacterStateController.Animator.enabled) return;
            if (RuntimeAnimator == null) return;

            string objectName = CharacterStateController.gameObject.name;
            if (objectName.Equals("NPC Template 2(Clone)", StringComparison.OrdinalIgnoreCase))
            {
                CharacterStateController.Animator.SetFloat(_blendMove, .5f);
            }
            else if (objectName.Equals("NPC Template 3(Clone)", StringComparison.OrdinalIgnoreCase))
            {
                CharacterStateController.Animator.SetFloat(_blendMove, .8f);
            }
            else if (_ranking.npcType == NpcType.BossNpc)
            {
                CharacterStateController.Animator.SetFloat(_blendMove, .35f);
            }

            CharacterStateController.Animator.SetBool(_hitButNotDead, _isPushBack);
        }

        public override void CheckExitTransition()
        {
            if (_toDieState) CharacterStateController.EnqueueTransition<NpcDieState>();
            else if (_toIdleState) CharacterStateController.EnqueueTransition<NpcIdleState>();
        }

        public override void ExitStateBehaviour(float dt, CharacterState toState)
        {
            Messenger.RemoveListener<int, Vector3, float>(EventID.DetectNpc, OnDetected);
            _toDieState = false;
            _toIdleState = false;
        }

        private void OnDetected(int instanceId, Vector3 forcePos, float forceRadius)
        {
            if (instanceId == CharacterStateController.gameObject.GetInstanceID())
            {
                if (_ranking.IsHealthOut)
                {
                    _toDieState = true;
                    Messenger.RaiseMessage(EventID.NpcDiedNotify, _ranking.npcType);
                }
                forceInfo = new Tuple<Vector3, float>(forcePos, forceRadius);
                
                movementControl.StopMove();
                _isPushBack = true;
                _body.AddExplosionForce(100f, forceInfo.Item1, forceInfo.Item2, 4, ForceMode.Impulse);

                DelayAction(2, () =>
                {
                    _isPushBack = false;
                    _body.velocity = Vector3.zero;
                }).Forget();
            }
        }
    }
}

