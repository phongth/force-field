using System.Collections;
using System.Collections.Generic;
using Base.MessageSystem;
using Base.Module;
using Base.Pattern;
using UltimateSpawner;
using UnityEngine;

namespace Game
{
    public class NpcDieState : CharacterState
    {
        [SerializeField] private NpcMovementControl movementControl;
        [SerializeField] private Color colorChangeTo;
        [SerializeField] private SkinnedMeshRenderer mesh;
        private Rigidbody _body;
        private NpcRanking _ranking;
        private bool _toIdleState = false;

        private Color _defaultColor;

        protected override void Start()
        {
            _body = this.GetComponentInBranch<Rigidbody>();
            _ranking = this.GetComponentInBranch<NpcRanking>();
        }

        public override void EnterState(float dt, CharacterState fromState)
        {
            if (fromState is NpcWanderingState wanderingState)
            {
                var material = mesh.material;
                _defaultColor = material.color;
                material.color = colorChangeTo;
                DelayAction(2, () => _toIdleState = true).Forget();
            } 
        }

        public override void FixedUpdateBehaviour(float dt)
        {
            return;
        }

        public override void CheckExitTransition()
        {
            if (_toIdleState) CharacterStateController.EnqueueTransition<NpcIdleState>();
        }

        public override void ExitStateBehaviour(float dt, CharacterState toState)
        {
            mesh.material.color = _defaultColor;
            UltimateSpawning.Despawn(CharacterStateController.gameObject);
        }
    }
}

