using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class CharacterJumpState : CharacterState
    {
        [SerializeField] private MovementControl movementControl;
        [SerializeField] private ForceControl forceControl;
        [SerializeField] private ParticleSystem landingFx;
        private bool _toMainState = false;
        
        private Vector3 _posClickOld;
        private Vector3 _deltaClick;
        private Vector3 _lookDir;

        // Dragging
        private float _distance;
        private Vector3 _offset;
        private bool _isDrag = false;
        private bool _isLanding = false;

        private static int _landing = Animator.StringToHash("Landing");

        public override void EnterState(float dt, CharacterState fromState)
        {
            //Messenger.RegisterListener<Vector3, InputPhase>(SystemMessage.Input, OnInputHandler);
            
            movementControl.OnLanding += OnLanding;
            movementControl.ExecuteJump(forceControl.Force);
        }

        public override void FixedUpdateBehaviour(float dt)
        {
            return;
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (!CharacterStateController.Animator) return;
            if (!CharacterStateController.Animator.enabled) return;
            if (RuntimeAnimator == null) return;

            if (_isLanding)
            {
                CharacterStateController.Animator.SetTrigger(_landing);
                _isLanding = false;
            }
        }

        public override void CheckExitTransition()
        {
            if (_toMainState && !_isLanding) CharacterStateController.EnqueueTransition<CharacterMainState>();
        }

        public override void ExitStateBehaviour(float dt, CharacterState toState)
        {
            //Messenger.RemoveListener<Vector3, InputPhase>(SystemMessage.Input, OnInputHandler);
            
            _toMainState = false;
            movementControl.OnLanding -= OnLanding;
        }

        private void OnLanding()
        {
            if (CharacterStateController.CurrentState is CharacterJumpState)
            {
                //landingFx.Play(true);
                _isLanding = true;
                forceControl.ForceRelease();
                forceControl.ForceDetect();
                _toMainState = true;
                movementControl.ResetScale();
            }
        }
    }
}

