using System.Collections;
using System.Collections.Generic;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class CharacterMainState : CharacterState
    {
        [SerializeField] private MovementControl movementControl;
        [SerializeField] private ForceControl forceControl;
        [SerializeField] private PlayerParams property;
        
        private Vector3 _posClickOld;
        private Vector3 _deltaClick;
        private Vector3 _lookDir;

        private bool _toJumpState = false;
        private bool _toDieState = false;

        private static int IsRun = Animator.StringToHash("IsRun");

        public override void EnterState(float dt, CharacterState fromState)
        {
            Messenger.RegisterListener<Vector3, InputPhase>(SystemMessage.Input, OnInputHandler);
        }

        public override void FixedUpdateBehaviour(float dt)
        {
            if (forceControl.IsForceHold)
            {
                movementControl.ExecuteMove();
            }
        }

        public override void PostUpdateBehaviour(float dt)
        {
            if (!CharacterStateController.Animator) return;
            if (!CharacterStateController.Animator.enabled) return;
            if (RuntimeAnimator == null) return;
            
            CharacterStateController.Animator.SetBool(IsRun, forceControl.IsForceHold);
        }

        public override void UpdateBehaviour(float dt)
        {
            if (property.health.StatValue <= 0)
            {
                _toDieState = true;
            }
        }

        public override void CheckExitTransition()
        {
            if (_toJumpState) CharacterStateController.EnqueueTransition<CharacterJumpState>();
            else if (_toDieState) CharacterStateController.EnqueueTransition<CharacterDieState>();
        }

        public override void ExitStateBehaviour(float dt, CharacterState toState)
        {
            Messenger.RemoveListener<Vector3, InputPhase>(SystemMessage.Input, OnInputHandler);

            _toJumpState = false;
            _toDieState = false;
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            _toJumpState = false;
            Messenger.RemoveListener<Vector3, InputPhase>(SystemMessage.Input, OnInputHandler);
        }

        private void OnInputHandler(Vector3 position, InputPhase phase)
        {
            switch (phase)
            {
                case InputPhase.Began:
                    _posClickOld = position;
                    forceControl.StartExpanding();

                    // Ray ray = Camera.main.ScreenPointToRay(position);
                    //
                    // if (Physics.Raycast(ray, out RaycastHit hit, 20f, LayerMask.GetMask("Player Related")))
                    // {
                    //     if (hit.collider.CompareTag("Dragable"))
                    //     {
                    //         _distance = Position.z - Camera.main.transform.position.z;
                    //         v3 = new Vector3(position.x, position.y, _distance);
                    //         v3 = Camera.main.ScreenToWorldPoint(v3);
                    //         _offset = Position - v3;
                    //         _isDrag = true;
                    //     }
                    // }

                    break;
                case InputPhase.Moved:
                    _deltaClick = position - _posClickOld;
                    _posClickOld = position;
                    forceControl.IsForceHold = true;

                    // if (_isDrag)
                    // {
                    //     v3 = new Vector3(position.x, position.y, _distance);
                    //     v3 = Camera.main.ScreenToWorldPoint(v3);
                    //     movementControl.ExecuteDrag(v3 + _offset);
                    // }
                    break;
                case InputPhase.Ended:
                    _deltaClick = Vector3.zero;
                    _lookDir = Vector3.zero;
                    forceControl.IsForceHold = false;
                    //_isDrag = false;
                    _toJumpState = true;
                    break;
                default:
                    break;
            }
            
            movementControl.ExecuteRotate(_deltaClick, _lookDir);
        }
    }
}

