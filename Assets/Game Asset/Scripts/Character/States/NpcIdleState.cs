using System.Collections;
using System.Collections.Generic;
using Base.Pattern;
using UnityEngine;

namespace Game
{
    public class NpcIdleState : CharacterState
    {
        
        public override void FixedUpdateBehaviour(float dt)
        {
            return;
        }

        public override void CheckExitTransition()
        {
            if (GameStateParam.IsGameRunning) CharacterStateController.EnqueueTransition<NpcWanderingState>();
        }
    }
}

