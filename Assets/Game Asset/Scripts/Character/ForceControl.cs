using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using Base;
using Base.MessageSystem;
using Base.Module;
using Base.Pattern;

namespace Game
{
    public class ForceControl : BaseMono
    {
        [SerializeField] private PlayerParams property;
        [SerializeField] private Transform forceSphere;
        [SerializeField] private Destructable forceBroken;
        [SerializeField] private Transform forceRelease;
        [SerializeField] private ParticleSystem forceFx;
        [SerializeField] private ParticleSystem releaseFx;

        private Vector3 _smoothSpd = Vector3.zero;
        private bool _isForceHold;
        private PlayerProgressControl _progressControl;
        private Rigidbody _body;
        private MovementControl _movementControl;
        private CharacterStateController _characterStateController;
        
        public float SpeedDamp { get; private set; }

        public bool IsForceHold
        {
            get => _isForceHold;
            set
            {
                var isForceHold = _isForceHold;
                if (isForceHold != value)
                {
                    _isForceHold = value;
                    if (!value) ForceReset();
                    if (value) forceRelease.localScale = Vector3.one * .1f;
                }
            }
        }
        public Vector3 Force { get; set; }

        private void Start()
        {
            _progressControl = GetComponent<PlayerProgressControl>();
            _body = GetComponent<Rigidbody>();
            _movementControl = GetComponent<MovementControl>();
            _characterStateController = GetComponent<CharacterStateController>();
        } 

        private void Update()
        {
            if (IsForceHold)
            {
                ForceExpanding();
            }
        }

        public void OnForceHitWhileCreated(GameObject source, Collider other)
        {
            if (other.CompareTag("NPC"))
            {
                if (property.health.StatValue <= 0) return;
                if (_characterStateController.CurrentState is CharacterJumpState) return;
                if (other.GetComponent<NpcRanking>().Health <= 0) return;
                //ForceReset();
                
                Destructable forceDestruct = Instantiate(forceBroken, forceSphere.position, forceBroken.Rotation);
                forceDestruct.Scale = forceSphere.localScale;
                forceDestruct.Explode(1000f, forceDestruct.Position, 50f, .5f, false);
                
                _movementControl.ResetScale();
                IsForceHold = false;

                property.health.Downgrade();
                if (property.health.StatValue <= 0)
                {
                    Messenger.RaiseMessage(EventID.GameOver, false);
                }
            }
        }

        private void ForceReset()
        {
            Force = forceSphere.localScale;
            forceSphere.localScale = Vector3.one * .2f;
            forceFx.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);
        }

        private void ForceExpanding()
        {
            if (!forceFx.isPlaying)
            {
                forceFx.Play(true);
            }
            forceRelease.gameObject.SetActive(false);
            Vector3 preScale = forceSphere.localScale;
            Vector3 desireScale = new Vector3(preScale.x * 2f, preScale.x * 2f, preScale.z * 2f);
            desireScale.x = Mathf.Clamp(desireScale.x, .1f, property.forceLimit.StatValue);
            desireScale.z = Mathf.Clamp(desireScale.z, .1f, property.forceLimit.StatValue);
            desireScale.y = Mathf.Clamp(desireScale.y, .1f, property.forceLimit.StatValue);

            Vector3 newScale = Vector3.SmoothDamp(preScale, desireScale, ref _smoothSpd, property.forceExpandingSpeed.StatValue);

            Vector3 forceFxScale = newScale / 2f;
            forceFxScale.x = Mathf.Clamp(forceFxScale.x, 1f, 3f);
            forceFxScale.y = Mathf.Clamp(forceFxScale.y, 1f, 3f);
            forceFxScale.z = Mathf.Clamp(forceFxScale.z, 1f, 3f);

            forceFx.transform.localScale = forceFxScale;

            forceSphere.localScale = newScale;
            SpeedDamp -= Time.fixedDeltaTime;
            SpeedDamp = Mathf.Clamp(SpeedDamp, 1, 3f);
        }

        public void StartExpanding()
        {
            IsForceHold = true;
            forceSphere.localScale = property.forceInitialSize.StatValue * Vector3.one;
            SpeedDamp = 2f;
            forceFx.Play(true);
        }

        public void ForceRelease()
        {
            Vector3 forceScale = Force * Constant.ForceMultiplier;
            
            // forceRelease.gameObject.SetActive(true);
            //  forceRelease.CacheTransform.DOScale(forceScale.x, .2f).SetEase(Ease.OutCirc)
            //      .OnComplete(() =>
            //      {
            //          forceRelease.Active = false;
            //      });

            Vector3 fxScale = forceScale / 2f;
            fxScale.x = Mathf.Clamp(fxScale.x, 1f, property.forceLimit.StatValue / 2f);
            fxScale.y = Mathf.Clamp(fxScale.y, 1f, property.forceLimit.StatValue / 2f);
            fxScale.z = Mathf.Clamp(fxScale.z, 1f, property.forceLimit.StatValue / 2f);

            releaseFx.transform.localScale = fxScale;
            releaseFx.Play(true);
            
            Taptic.Taptic.Vibrate();
        }

        public void ForceDetect()
        {
            var result = Physics.OverlapSphere(forceRelease.position, Force.x * (Constant.ForceMultiplier / 2), 
                LayerMask.GetMask("Npc", "Obstacle"));
            if (result.Length > 0)
            {
                for (int i = 0; i < result.Length; ++i)
                {
                    Transform target = result[i].transform;
                    if (target.CompareTag("NPC"))
                    {
                        NpcRanking npcRanking = target.GetComponentInBranch<CharacterStateController, NpcRanking>();
                        Messenger.RaiseMessage(EventID.DetectNpc, target.gameObject.GetInstanceID(), forceRelease.position, 
                            Force.x * (Constant.ForceMultiplier / 2));
                    }
                    else if (target.CompareTag("Obstacle"))
                    {
                        Destructable destructable = target.GetComponent<Destructable>();
                        destructable.Explode(200f, destructable.Position, 20f);
                    }
                }
            }
        }
    }
}

