using System;
using Base;
using Base.MessageSystem;
using UnityEngine;

namespace Game
{
    public class PlayerProgressControl : BaseMono
    {
        [SerializeField] private PlayerParams property;
        private void OnEnable()
        {
            Messenger.RegisterListener<NpcType>(EventID.NpcDiedNotify, OnNpcDiedHandler);
            
            property.Load();
            property.ResetDefault();
        }

        private void OnDisable()
        {
            Messenger.RemoveListener<NpcType>(EventID.NpcDiedNotify, OnNpcDiedHandler);
            
            property.Save();
        }

        private void OnNpcDiedHandler(NpcType npcType)
        {
            if (npcType == NpcType.NormalNpc)
            {
                property.money += 10;
            }
            else
            {
                property.money += 100;
                Messenger.RaiseMessage(EventID.GameOver, true);
            }
        }
    }
}

