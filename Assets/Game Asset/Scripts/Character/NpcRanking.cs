using System;
using Base;
using Base.MessageSystem;
using UltimateSpawner;

namespace Game
{
    public class NpcRanking : BaseMono
    {
        public NpcType npcType;
        public NpcNormalType npcNormalType;
        public int health;

        public bool IsHealthOut
        {
            get
            {
                health -= 1;
                return health <= 0;
            }
        }

        public int Health => health;
    }
    
    public enum NpcType {NormalNpc, BossNpc}
    public enum NpcNormalType {None = -1, Npc1 = 0, Npc2 = 3, Npc3 = 1}
}

