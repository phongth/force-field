using System;
using DG.Tweening;
using Base;
using UnityEngine;
using UnityEngine.AI;

namespace Game
{
    public class NpcMovementControl : BaseMono
    {
        [SerializeField] private Rigidbody body;
        [SerializeField] private float moveSpeed;
        [SerializeField] private NavMeshAgent agent;

        public float StoppingDistance => agent.stoppingDistance;

        public void ExecuteRotate(float angle)
        {
            EulerAngles = new Vector3(0, angle, 0);
        }

        public void ExecuteMove(Vector3 direction)
        {
            agent.velocity = direction.normalized * (moveSpeed * Time.fixedDeltaTime);
        }

        public void StopMove()
        {
            agent.velocity = Vector3.zero;
        }
    }
}

