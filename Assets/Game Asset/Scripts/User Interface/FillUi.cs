using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class FillUi : MonoBehaviour
    {
        [SerializeField] private Image fillImage;
        
        public int Total { get; set; }

        private void Start()
        {
            fillImage.fillAmount = 1;
        }

        public void Fill(int crr)
        {
            float amount = (float)crr / Total;
            fillImage.fillAmount = 1 - amount;
        }
    }
}

