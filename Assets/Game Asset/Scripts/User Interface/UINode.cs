using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Game
{
    public class UINode : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI valueText;

        public void SetValue(string value)
        {
            valueText.text = value;
        }
    }
}

