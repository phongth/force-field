using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.Module;
using UnityEngine;

namespace Game
{
    public class NavigationIndicator : BaseMono
    {
        [SerializeField] private RectTransform indicator;
        [SerializeField] private RectTransform limitPanel;

        private Transform _target;
        private BaseMono _mc;
        private Camera _mainCamera;

        private void Start()
        {
            _mainCamera = Camera.main;
        }

        public void SetTarget(Transform target, BaseMono mc)
        {
            _target = target;
            _mc = mc;
        }

        public void UpdateIndicator()
        {
            if (!_target || !_mc || !_mainCamera) return;

            Vector3 targetPos = _target.position;
            bool isView = UtilsClass.IsInCameraView(_mainCamera, targetPos);
            gameObject.SetActive(!isView);
            if (!isView)
            {
                Vector3 newPos = _mainCamera.WorldToScreenPoint(targetPos);
                newPos.x = Mathf.Clamp(newPos.x, RectTransform.sizeDelta.x / 2f,
                    limitPanel.rect.width);
                newPos.y = Mathf.Clamp(newPos.y, RectTransform.sizeDelta.y / 2f,
                    limitPanel.rect.height);
                newPos = limitPanel.TransformVector(newPos.x, newPos.y, newPos.z);
                RectTransform.position = newPos;

                Vector3 targetDirection = (targetPos - _mc.Position).normalized;
                Quaternion targetRotation = Quaternion.LookRotation(targetDirection);
                Quaternion result = 
                    Quaternion.RotateTowards(RectTransform.rotation, targetRotation, Time.deltaTime * 8000f);
                
                result.z = -1f * result.y;
                result.y = 0;

                indicator.rotation = result;
            }
        }
    }
}

