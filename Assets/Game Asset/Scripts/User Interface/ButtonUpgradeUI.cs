using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class ButtonUpgradeUI : MonoBehaviour
    {
        [SerializeField] private UpgradeItem upgradeItem;
        [SerializeField] private PlayerParams playerParam;
        [SerializeField] private UpgradeType type;
        [SerializeField] private Sprite iconSprite;

        [Header("UI"), SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI price;
        [SerializeField] private Image icon;
        [SerializeField] private Image popup;
        [SerializeField] private Image coin;

        [Header("Sprite Ref"), SerializeField] private Sprite onPopup;
        [SerializeField] private Sprite offPopup;
        [SerializeField] private Sprite onCoin;
        [SerializeField] private Sprite offCoin;

        private void Start()
        {
            Load();
            UpdateButton();
        }

        private void Save()
        {
            PlayerPrefs.SetInt(type.ToString(), (int) upgradeItem.Level);
            PlayerPrefs.Save();
        }

        private void Load()
        {
            int level = PlayerPrefs.GetInt(type.ToString(), 1);
            upgradeItem.Level = (uint) level;
            switch (type)
            {
                case UpgradeType.Health: 
                    playerParam.health.InitCountChange(level - 1);
                    break;
                case UpgradeType.ForceExpandingSpeed:
                    playerParam.forceExpandingSpeed.InitCountChange(level - 1);
                    break;
                case UpgradeType.ForceInitializeSize:
                    playerParam.forceLimit.InitCountChange(level - 1);
                    break;
            }
            
        }

        private void UpdateButton()
        {
            string typeName = String.Empty;
            switch (type)
            {
                case UpgradeType.Health: typeName = "Health";
                    break;
                case UpgradeType.ForceExpandingSpeed:
                    typeName = "Field Speed";
                    break;
                case UpgradeType.ForceInitializeSize:
                    typeName = "Field Size";
                    break;
            }
            string titleText = $"<color=\"white\"><size=\"35%\">{typeName}</size></color>\n " +
                               $"<color=\"black\"><size=\"20%\">Level {upgradeItem.Level}</size></color>";
            title.text = titleText;
            price.text = upgradeItem.CurrentPrice.ToString();
            icon.sprite = iconSprite;

            if (playerParam.money >= upgradeItem.CurrentPrice)
            {
                popup.sprite = onPopup;
                coin.sprite = onCoin;
            }
            else
            {
                popup.sprite = offPopup;
                coin.sprite = offCoin;
            }
        }

        public void BuyClick()
        {
            if (playerParam.money >= upgradeItem.CurrentPrice && upgradeItem.Level <= 10)
            {
                playerParam.money -= upgradeItem.CurrentPrice;
                upgradeItem.Buy();

                switch (type)
                {
                    case UpgradeType.Health:
                        playerParam.health.Upgrade();
                        break;
                    case UpgradeType.ForceExpandingSpeed:
                        playerParam.forceExpandingSpeed.Upgrade();
                        break;
                    case UpgradeType.ForceInitializeSize:
                        playerParam.forceLimit.Upgrade();
                        break;
                }
                
                UpdateButton();
                Save();
            }
            else
            {
                
            }
        }
    }
}

