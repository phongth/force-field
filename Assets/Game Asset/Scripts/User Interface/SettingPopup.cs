using System;
using System.Collections;
using System.Collections.Generic;
using Base;
using Base.Module;
using UnityEngine;

namespace Game
{
    public class SettingPopup : BaseMono
    {
        private SettingConfig _settingConfig;

        [SerializeField] private BaseButton sound;
        [SerializeField] private BaseButton vibrate;
        
        private void Awake()
        {
            if (SaveLoad.LoadFromJson(out SettingConfig result, "setting.json"))
            {
                _settingConfig = new SettingConfig(result);
            }
            else
            {
                _settingConfig = new SettingConfig();
            }
        }

        private void OnEnable()
        {
            sound.Initialize(_settingConfig.IsMusic);
            vibrate.Initialize(_settingConfig.IsVibrate);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            
            SaveLoad.SaveToJson(_settingConfig, "setting.json");
        }

        public void OnSoundClick(bool isOn)
        {
            _settingConfig.IsMusic = isOn;
        }

        public void OnVibrateClick(bool isOn)
        {
            _settingConfig.IsVibrate = isOn;
            Taptic.Taptic.tapticOn = isOn;
        }
    }
}

