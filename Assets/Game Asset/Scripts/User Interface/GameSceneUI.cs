using System;
using Base;
using Base.MessageSystem;
using Base.Pattern;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameSceneUI : BaseMono
    {
        [SerializeField] private GameObject startUi;
        [SerializeField] private GameObject playPanel;
        [SerializeField] private GameObject upgradePopup;
        [SerializeField] private UINode moneyNode;
        [SerializeField] private UINode healthNode;
        [SerializeField] private UINode worldTitleNode;
        [SerializeField] private FillUi numEnemyNode;
        [SerializeField] private GameObject winNoti, loseNoti, resultNoti;

        [SerializeField] private UINode totalKillNode;
        [SerializeField] private UINode moneyEarnNode;

        [Space, SerializeField] private PlayerParams playerParams;
        
        private GameStateController _gameStateController;
        private GameSceneController _gameSceneController;
        private LevelController _levelController;

        private uint _lastMoney;

        private void Awake()
        {
            _gameStateController = GameObject.FindGameObjectWithTag("Game State Controller").GetComponent<GameStateController>();

            _gameStateController.OnStateChanged += OnGameStateChanged;
        }

        private void Start()
        {
            var gameSceneRoot = SceneManager.GetSceneByName("Game Scene").GetRootGameObjects();
            _gameSceneController = gameSceneRoot[0].GetComponent<GameSceneController>();
            _levelController = _gameSceneController.LevelController;

            numEnemyNode.Total = Constant.NumOfEnemyBeforeBoss * _gameSceneController.GameLevel;
        }

        private void LateUpdate()
        {
            int currEnemyDie = _levelController.TotalNpcDie;
            moneyNode.SetValue($"{playerParams.money}");
            healthNode.SetValue($"{playerParams.health.StatValue}");
            numEnemyNode.Fill(currEnemyDie);
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();
            _gameStateController.OnStateChanged -= OnGameStateChanged;
        }

        private void OnGameStateChanged(GameState from, GameState to)
        {
            if (to is GameInitializeState)
            {
                winNoti.SetActive(false);
                loseNoti.SetActive(false);
                resultNoti.SetActive(false);
                playPanel.SetActive(false);
                
                startUi.SetActive(true);
            }
            else if (to is GameRunningState)
            {
                startUi.SetActive(false);
                playPanel.SetActive(true);
                worldTitleNode.SetValue($"World {_gameSceneController.GameLevel}");

                _lastMoney = playerParams.money;
            }
            else if (to is GameEndState)
            {
                if (from is GameRunningState runningState)
                {
                    int currEnemyDie = _gameSceneController.LevelController.TotalNpcDie;
                    playPanel.SetActive(false);
                    bool isWin = runningState.IsGameWin;
                    ShowEndResult(isWin);
                    totalKillNode.SetValue($"Kills {currEnemyDie}");

                    uint crrMoney = playerParams.money - _lastMoney;
                    moneyEarnNode.SetValue($"+{crrMoney}");
                }
            }
        }

        private void ShowEndResult(bool isWin)
        {
            winNoti.SetActive(isWin);
            loseNoti.SetActive(!isWin);
            resultNoti.SetActive(true);
        }

        public void ReplayClick(bool isWin)
        {
            Messenger.RaiseMessage(EventID.Replay, isWin);
        }

        public void ReturnHome()
        {
            Messenger.RaiseMessage(EventID.ToHome);
        }
    }
}

