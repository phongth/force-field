using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Game
{
    public class BaseButton : MonoBehaviour
    {
        [SerializeField] private Sprite offSprite;
        [SerializeField] private Sprite onSprite;
        [SerializeField] private Image image;
        [SerializeField] private Button button;
        [SerializeField] private UnityEvent<bool> callback;

        private bool _value = true;

        private void Awake()
        {
            image = GetComponentInChildren<Image>();
            button = GetComponentInChildren<Button>();

            button.onClick.AddListener(OnButtonClick);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(OnButtonClick);
        }

        private void OnButtonClick()
        {
            _value = !_value;
            image.sprite = _value ? onSprite : offSprite;
            callback?.Invoke(_value);
        }

        public void Initialize(bool isOn)
        {
            _value = isOn;
            image.sprite = _value ? onSprite : offSprite;
        }
    }
}

