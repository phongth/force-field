using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public enum EventID {DetectNpc, GameOver, NpcDiedNotify, Replay, ToHome}
    
    public enum UpgradeType {Health, ForceInitializeSize, ForceExpandingSpeed}

    public static class Constant
    {
        public const float ForceMultiplier = 1.25f;
        public const float JumpMultiplier = .4f;
        public const int NumOfEnemyBeforeBoss = 50;
    }
    
    [System.Serializable]
    public class SettingConfig
    {
        public bool IsMusic = true;
        public bool IsVibrate = true;
        
        public SettingConfig() {}

        public SettingConfig(SettingConfig source)
        {
            IsMusic = source.IsMusic;
            IsVibrate = source.IsVibrate;
        }
    }
    
    public static class GameStateParam
    {
        public static bool IsGameRunning = false;
    }
}

