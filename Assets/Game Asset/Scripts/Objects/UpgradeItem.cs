
using UnityEngine;

namespace Game
{   
    [System.Serializable]
    public class UpgradeItem
    {
        [SerializeField] private uint basePrice;
        private uint level = 1;

        public uint CurrentPrice => basePrice * level;
        public uint Level
        {
            get => level;
            set => level = value;
        }

        public void Buy()
        {
            level += 1;
            level = (uint) Mathf.Clamp(level, 1, 10);
        }
    }
}

