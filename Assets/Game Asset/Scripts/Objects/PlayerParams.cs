using System;
using Base;
using Base.Module;
using UnityEngine;

namespace Game
{
    [CreateAssetMenu(menuName = "Game/Player Property")]
    public class PlayerParams : ScriptableObject
    {
        public float moveSpeed;
        public float jumpSpeed;
        public float speedLerpRotate;
        public Stat forceLimit;
        
        public Stat forceExpandingSpeed;
        public Stat forceInitialSize;
        public Stat health;
        public uint money;

        public void ResetDefault()
        {
            health.Reset();
            forceExpandingSpeed.Reset();
            forceInitialSize.Reset();
            forceLimit.Reset();
        }

        public void Save()
        {
            SaveLoad.SaveToBinary(money, "purse.bin");
        }

        public void Load()
        {
            SaveLoad.LoadFromBinary(out uint result, "purse.bin");
            money = result;
        }
    }
}

